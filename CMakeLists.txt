cmake_minimum_required (VERSION 2.8)

project (ttsclipboard)

# Turn on the ability to create folders to organize projects (.vcproj)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)


# Here it is setting the Visual Studio warning level to 4
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")

# Default to statically-linked runtime.
foreach(flag_var
        CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
        CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO)
   if(${flag_var} MATCHES "/MD")
      string(REGEX REPLACE "/MD" "/MT" ${flag_var} "${${flag_var}}")
   endif(${flag_var} MATCHES "/MD")
endforeach(flag_var)

### Sources, headers, directories and libs
file(GLOB sources "src/*.cpp")
file(GLOB sources_stdafx "src/stdafx/*.cpp")
file(GLOB private_headers "src/*.h")
file(GLOB res_files "src/res/*.*")


### add include dir
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src/res")



	

add_executable(ttsclipboard  WIN32  ${sources_stdafx} ${sources} ${private_headers} ${res_files})
#####################Resource Files
source_group("Resource Files" FILES ${res_files})
add_definitions(-DUNICODE -D_UNICODE)

if (MSVC)
	foreach( src_file ${sources_stdafx} )		
		set_source_files_properties( ${src_file} PROPERTIES COMPILE_FLAGS "/Ycstdafx.h")
    endforeach( src_file ${sources_stdafx} )
	
    foreach( src_file ${sources} )		
		set_source_files_properties( ${src_file} PROPERTIES COMPILE_FLAGS "/Yustdafx.h")
    endforeach( src_file ${sources} )
endif (MSVC)
