#include "stdafx.h"
#include "voices.h"

#include "ttsclipboard.h"


INT_PTR CALLBACK    DlgMsgs(HWND, UINT, WPARAM, LPARAM);



void StartVoicesDlg(HINSTANCE hInstance, HWND hParent)
{
   ::DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG_VOICE), hParent, reinterpret_cast<DLGPROC>(DlgMsgs));
}

bool AddVoiceToCombBoxCallback(LPCWSTR szVoiceName, ISpObjectToken* pSpTok, HWND hwnd, void* pData)
{
   UNREFERENCED_PARAMETER(pSpTok);
   UNREFERENCED_PARAMETER(pData);

   SendMessage(hwnd, CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(szVoiceName));

   return false;
}



INT_PTR CALLBACK DlgMsgs(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
   UNREFERENCED_PARAMETER(lParam);
   switch (message)
   {
      case WM_INITDIALOG:
      {
         HWND hwndCB = ::GetDlgItem(hDlg, IDC_COMBO_VOICES); _ASSERT(hwndCB);
         EnumVoice(AddVoiceToCombBoxCallback, hwndCB, nullptr);
         SendMessage(hwndCB, CB_SETCURSEL, 0, 0);

      }return (INT_PTR)TRUE;

      case WM_CLOSE:
      {
         EndDialog(hDlg, IDCANCEL);
      }return (INT_PTR)TRUE;


   case WM_COMMAND:

      switch (LOWORD(wParam))
      {
         case IDOK:
         {
            HWND hwndCB = ::GetDlgItem(hDlg, IDC_COMBO_VOICES); _ASSERT(hwndCB);
            int nIdx = ::SendMessage(hwndCB, CB_GETCURSEL, 0, 0);
            if (nIdx != CB_ERR)
            {
               int nSize = ::SendMessage(hwndCB, CB_GETLBTEXTLEN, nIdx, 0);
               if (nSize != CB_ERR)
               {
                  nSize++;
                  wchar_t *pTxt = new wchar_t[nSize];
                  memset(pTxt, 0, sizeof(wchar_t)*nSize);
                  if (::SendMessage(hwndCB, CB_GETLBTEXT, nIdx, reinterpret_cast<LPARAM>(pTxt)) != CB_ERR)
                  {
                     SetDefaultVoice(pTxt, false);
                  }                  
                  delete[] pTxt;
               }
            }
         }
         case IDCANCEL:
            EndDialog(hDlg, LOWORD(wParam));
      }
      return (INT_PTR)TRUE;
      
   }
      
   return (INT_PTR)FALSE;
}