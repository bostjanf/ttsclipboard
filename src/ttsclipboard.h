#pragma once

#include "resource.h"

typedef bool(*CallbackType)(LPCWSTR szVoiceName, ISpObjectToken* pSpTok, HWND hwnd, void* pData);

bool EnumVoice(CallbackType callback, HWND hwnd, void* pData);
bool SetDefaultVoice(LPCWSTR szVoiceName, bool bWriteToFile);
