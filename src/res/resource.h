//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ttsclipboard.rc
//
#define IDC_MYICON                      2
#define IDD_DIALOG_MAIN                 9
#define IDD_TTSCLIPBOARD_DIALOG         102
#define IDM_ABOUT                       104
#define IDI_TTSCLIPBOARD                107
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     131
#define IDD_DIALOG2                     132
#define IDD_DIALOG_VOICE                132
#define IDC_BUTTON_PLAY                 1000
#define IDC_BUTTON_STOP                 1001
#define IDC_BUTTON_CLIPBOARD            1002
#define IDC_BUTTON_VOICE                1003
#define IDC_SLIDER1                     1006
#define IDC_COMBO_VOICES                1010
#define IDC_STATIC                      -1
#define IDC_STATIC_VOICE                -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
