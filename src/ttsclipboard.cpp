// ttsclipboard.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "ttsclipboard.h"
#include "voices.h"



// Global Variables:
HWND g_hLabelSelVoice = NULL;
HWND g_hButtPlay = NULL;
HWND g_hButtStop = NULL;

const size_t MAX_CLIPBOARD_STRING_LEN = 1024 * 1024 * 20;
wchar_t*  g_pTxt = nullptr;
ISpVoice * g_pVoice = nullptr;
HINSTANCE hInst; // current instance
BOOL                InitInstance(HINSTANCE, int);
INT_PTR CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

   if (FAILED(::CoInitialize(NULL)))
      return FALSE;

    
    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }


    
    MSG msg;
    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
         TranslateMessage(&msg);
         DispatchMessage(&msg);
    }

    if (g_pVoice)
    {
       g_pVoice->Release();
       g_pVoice = nullptr;
    }

    ::CoUninitialize();
    return (int) msg.wParam;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   int cx = 0, cy = 0; 
   hInst = hInstance; // Store instance handle in our global variable
   HWND hWnd = CreateDialogParam(hInstance, MAKEINTRESOURCE(IDD_DIALOG_MAIN), 0, WndProc,0);

   if (!hWnd)
   {
      return FALSE;
   }
   
   //set icon
   HICON hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TTSCLIPBOARD));
   SendMessage(hWnd, WM_SETICON, WPARAM(ICON_SMALL), LPARAM(hIcon));

   //position to middle of screen
   cx = GetSystemMetrics(SM_CXSCREEN);
   cy = GetSystemMetrics(SM_CYSCREEN);
   if (cx && cy)
   {
      RECT rcDlg;
      if (GetWindowRect(hWnd, &rcDlg))
      {
         int cxPos = cx, cyPos = cy;
         int cxHalfScreen = cx / 2, cyHalfScreen = cy / 2;
         int cxHalfDlg = rcDlg.right / 2, cyHalfDlg = rcDlg.bottom / 2;

         if (cxHalfScreen > cxHalfDlg)
         {
            cxPos = cxHalfScreen - cxHalfDlg;
         }
         if (cyHalfScreen > cyHalfDlg)
         {
            cyPos = cyHalfScreen - cyHalfDlg;
         }
         SetWindowPos(hWnd, HWND_TOP, cxPos, cyPos, 0, 0, SWP_NOSIZE);
      }
   }

   ShowWindow(hWnd, nCmdShow);
   return TRUE;
}

bool IsAppSpeaking()
{
   if (g_pVoice)
   {
      SPVOICESTATUS stat;
      g_pVoice->GetStatus(&stat, nullptr);
      if (stat.dwRunningState == SPRS_IS_SPEAKING)
      {
         return true;
      }
   }
   return false;
}

bool EnumVoice(CallbackType callback, HWND hwnd, void* pData)
{
   bool bRes = false;
   HRESULT hr = S_OK;   
   _ASSERT(callback);
   if (callback == nullptr)
   {
      return false;
   }

   ISpObjectTokenCategory* cpSpCategory = nullptr;
   if (SUCCEEDED(hr = SpGetCategoryFromId(SPCAT_VOICES, &cpSpCategory)))
   {
      IEnumSpObjectTokens* cpSpEnumTokens = nullptr;;
      if (SUCCEEDED(hr = cpSpCategory->EnumTokens(NULL, NULL, &cpSpEnumTokens)))
      {         
         ISpObjectToken* pSpTok = nullptr;
         while (SUCCEEDED(hr = cpSpEnumTokens->Next(1, &pSpTok, NULL)) && pSpTok && (bRes==false))
         {
            CSpDynamicString szDescription;
            if (SUCCEEDED(SpGetDescription(pSpTok, &szDescription)))
            {
               bRes = callback(szDescription, pSpTok, hwnd, pData);
            }
            pSpTok->Release();
         }
         cpSpEnumTokens->Release();
      }
      cpSpCategory->Release();
   }

   return bRes;
}

bool SetDefaultVoiceCallback(LPCWSTR szVoiceName, ISpObjectToken* pSpTok, HWND hwnd, void* pData)
{   
   LPCWSTR szSetAsDefault = (LPCWSTR)pData;
   if (g_pVoice && (lstrcmp(szVoiceName, szSetAsDefault) == 0) )
   {
      if (IsAppSpeaking()) { g_pVoice->Speak(nullptr, SPF_PURGEBEFORESPEAK, nullptr); }
      if (SUCCEEDED(g_pVoice->SetVoice(pSpTok)))
      {
         ::SetWindowTextW(hwnd, szVoiceName);
         return true;
      }
   }
   return false;
}

bool SetDefaultVoice(LPCWSTR szVoiceName, bool bWriteToFile)
{   
   _ASSERT(g_pVoice);
   bool bRes = EnumVoice(SetDefaultVoiceCallback, g_hLabelSelVoice, (void*)szVoiceName);
   if (bRes && bWriteToFile)
   {

   }
   return bRes;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
INT_PTR CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
   UNREFERENCED_PARAMETER(lParam);
   switch (message)
   {
   case WM_INITDIALOG:
   {     
      HRESULT hr = ::CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&g_pVoice);
      if (FAILED(hr))
      {
         ::MessageBox(hWnd, _T("Failed to init TTS engine."), _T("Fatal error"), MB_OK|MB_ICONERROR);
         PostQuitMessage(0);
         break;
      }

      g_hLabelSelVoice = GetDlgItem(hWnd, IDC_STATIC_VOICE); _ASSERT(g_hLabelSelVoice);
      g_hButtPlay = GetDlgItem(hWnd, IDC_BUTTON_PLAY); _ASSERT(g_hButtPlay);
      g_hButtStop = GetDlgItem(hWnd, IDC_BUTTON_STOP); _ASSERT(g_hButtStop);
      ::EnableWindow(g_hButtPlay, false);
      //::EnableWindow(g_hButtStop, false);
      if (g_hLabelSelVoice)
      {
         ISpObjectToken * pCurrVoice = nullptr;
         if (SUCCEEDED(g_pVoice->GetVoice(&pCurrVoice)))
         {
            CSpDynamicString szDescription;
            if (SUCCEEDED(SpGetDescription(pCurrVoice, &szDescription)))
            {
               ::SetWindowTextW(g_hLabelSelVoice, szDescription);
            }
            pCurrVoice->Release();
            pCurrVoice = nullptr;
         }
      }
      AddClipboardFormatListener(hWnd);
   }
   return TRUE;
   case WM_COMMAND:
      {
         int wmId = LOWORD(wParam);
         // Parse the menu selections:
         switch (wmId)
         {
            case IDM_ABOUT:
               //DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG_MAIN), hWnd, About);
            return TRUE;

            case IDC_BUTTON_VOICE:
               StartVoicesDlg(hInst, hWnd);
            return TRUE;

            case IDC_BUTTON_STOP:
               if (IsAppSpeaking()) { g_pVoice->Speak(nullptr, SPF_PURGEBEFORESPEAK, nullptr); }
            return TRUE;
         }
      }
   return TRUE;
   case WM_CLIPBOARDUPDATE:
   {
      static UINT auPriorityList[] = {CF_TEXT};
      if (::GetPriorityClipboardFormat(auPriorityList, sizeof(auPriorityList)/sizeof(UINT)) > 0)
      {
         ::SetTimer(hWnd, 123, 50, NULL);
      }
   }
   return TRUE;

   case WM_TIMER:
   {
      if (wParam == 123)
      {
         ::KillTimer(hWnd, 123);
         ::PostMessage(hWnd, MY_MSG_READ_CLIPBOARD, 0, 0);
      }
   }
   return TRUE;

   case MY_MSG_READ_CLIPBOARD:
   {
      static bool bOnceEnable = true;
      if (::OpenClipboard(hWnd))
      {
         HGLOBAL hglb = ::GetClipboardData(CF_TEXT);
         if (hglb)
         {
            LPCSTR lpstr = (LPCSTR)::GlobalLock(hglb);
            size_t nStrLen = strlen(lpstr);
            if (nStrLen && (nStrLen < MAX_CLIPBOARD_STRING_LEN))
            {//convert to Unicode
               int nSize = ::MultiByteToWideChar(CP_ACP, 0, lpstr, nStrLen, nullptr, 0);
               if (nSize)
               {
                  {//stop before del buff                    
                     if (IsAppSpeaking())
                     {
                        g_pVoice->Speak(nullptr, SPF_PURGEBEFORESPEAK, nullptr);
                     }
                  }
                  int nAlloc = nSize+1;
                  if (g_pTxt) { delete [] g_pTxt;}

                  g_pTxt = new wchar_t[nAlloc];
                  g_pTxt[nSize] = 0;//null at end

                  if (::MultiByteToWideChar(CP_ACP, 0, lpstr, nStrLen, g_pTxt, nSize) == nSize)
                  {
                     g_pVoice->Speak(g_pTxt, SPF_ASYNC|SPF_IS_NOT_XML, nullptr);
                  }

                  if (bOnceEnable)
                  {
                     bOnceEnable = false;
                     ::EnableWindow(g_hButtPlay, true);
                  }
               }
            }
            ::GlobalUnlock(hglb);
         }
         ::CloseClipboard();
      }
   }
   return TRUE;
   case WM_CLOSE:
      ::DestroyWindow(hWnd);
   return TRUE;
   case WM_DESTROY:
      if (IsAppSpeaking()){g_pVoice->Speak(nullptr, SPF_PURGEBEFORESPEAK, nullptr);}
      RemoveClipboardFormatListener(hWnd);
      PostQuitMessage(0);
      return TRUE;   
   }
   return FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
